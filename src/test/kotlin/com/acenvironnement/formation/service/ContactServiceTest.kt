package com.acenvironnement.formation.service

import com.acenvironnement.formation.domain.Adresse
import com.acenvironnement.formation.domain.Contact
import com.acenvironnement.formation.repository.ContactRepository
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.TemporalUnitLessThanOffset
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.repository.findByIdOrNull
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

@SpringBootTest
class ContactServiceTest {

    // Nécessaire, on ne peut pas utiliser l'injection dans le constructeur dans les TU
    @Autowired
    private lateinit var contactService: ContactService

    @Autowired
    private lateinit var contactRepository: ContactRepository

    @Test
    fun save() {
        // ETANT DONNÉ un contact en base
        val contact = Contact(
                nom = "nom",
                numTel = listOf("012345678"),
                adresse = listOf(Adresse("voie", "31000", "Toulouse"))
        )
        val contactInserted = contactRepository.save(contact)


        // LORSQUE je demande à modifier tous les champs du contact
        val contactUpdate = contact.copy(
                nom = "nom",
                numTel = listOf("012345678"),
                adresse = listOf(Adresse("voie", "31000", "Toulouse")),
                dateCreation = LocalDateTime.now().plusYears(10),
                dateDerniereMaj = LocalDateTime.now().plusYears(20),
        )
        contactService.save(contactUpdate)

        // ALORS tous les champs sont correctement modifiés en base
        val actualContact = contactRepository.findByIdOrNull(contactInserted.id)
        assertThat(actualContact).isNotNull()
        actualContact!!
        assertThat(actualContact.id).isEqualTo(contactInserted.id)
        assertThat(actualContact.nom).isEqualTo(contactUpdate.nom)
        assertThat(actualContact.numTel).containsExactlyElementsOf(contactUpdate.numTel)
        assertThat(actualContact.adresse).containsExactlyElementsOf(contactUpdate.adresse)
        assertThat(actualContact.dateCreation).isCloseTo(contact.dateCreation, TemporalUnitLessThanOffset(1, ChronoUnit.SECONDS))
        assertThat(actualContact.dateDerniereMaj).isAfter(contact.dateDerniereMaj)
    }


}
