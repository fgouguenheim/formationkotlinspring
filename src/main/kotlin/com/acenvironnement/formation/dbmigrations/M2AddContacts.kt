package com.acenvironnement.formation.dbmigrations

import com.acenvironnement.formation.domain.Adresse
import com.acenvironnement.formation.domain.Contact
import com.acenvironnement.formation.repository.ContactRepository
import com.github.cloudyrock.mongock.ChangeLog
import com.github.cloudyrock.mongock.ChangeSet

@ChangeLog(order = "002")
@Suppress("unused")
class M2AddContacts {

    @ChangeSet(order = "02", author = "initiator", id = "02-addContacts")
    fun addContacts(contactRepository: ContactRepository) {

        val contact1 = Contact(
            nom = "Tom Saywer",
            numTel = listOf("12311565"),
            adresse = listOf(Adresse("1 rue de la Paix", "75001", "Paris"))
        )

        val contact2 = Contact(
            nom = "Pif Hercule",
            numTel = listOf("12311565"),
            adresse = listOf(
                Adresse("1 rue de la Paix", "75001", "Paris"),
                Adresse("1 rue de la Paix", "31000", "Toulouse")
            )
        )

        contactRepository.saveAll(listOf(contact1, contact2))

    }
}
