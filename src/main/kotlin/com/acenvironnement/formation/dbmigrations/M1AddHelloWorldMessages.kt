package com.acenvironnement.formation.dbmigrations

import com.acenvironnement.formation.domain.HelloWorldMessage
import com.github.cloudyrock.mongock.ChangeLog
import com.github.cloudyrock.mongock.ChangeSet
import com.github.cloudyrock.mongock.driver.mongodb.springdata.v3.decorator.impl.MongockTemplate

@ChangeLog(order = "001")
@Suppress("unused")
class M1AddHelloWorldMessages {

    @ChangeSet(order = "01", author = "initiator", id = "01-addHelloWorldMessages")
    fun addHelloWorldMessages(mongoTemplate: MongockTemplate) {

        val helloMessage = HelloWorldMessage(
            text = "Hello"
        )
        mongoTemplate.save(helloMessage)

        val worldMessage = HelloWorldMessage(
            text = "World"
        )
        mongoTemplate.save(worldMessage)
    }
}
