package com.acenvironnement.formation.dbmigrations

import com.acenvironnement.formation.domain.Tag
import com.acenvironnement.formation.repository.ContactRepository
import com.acenvironnement.formation.repository.TagRepository
import com.github.cloudyrock.mongock.ChangeLog
import com.github.cloudyrock.mongock.ChangeSet

@ChangeLog(order = "003")
@Suppress("unused")
class M3AddTags {

    @ChangeSet(order = "01", author = "initiator", id = "03-addTags")
    fun addTags(tagRepository: TagRepository) {

        val tag1 = Tag(
            nom = "Amis"
        )

        val tag2 = Tag(
            nom = "Famille"
        )

        val tag3 = Tag(
            nom = "Travail"
        )

        tagRepository.saveAll(listOf(tag1, tag2, tag3))

    }

    @ChangeSet(order = "02", author = "initiator", id = "03-addContactsTags")
    fun addContactsTags(tagRepository: TagRepository, contactRepository: ContactRepository) {

        val tags: List<Tag> = tagRepository.findAll()

        contactRepository.findAll().forEach { contact ->
            contactRepository.save(contact.copy(
                tagsIds = tags.shuffled().subList(0, 2).mapNotNull { it.id }
            ))
        }

    }
}
