package com.acenvironnement.formation.repository

import com.acenvironnement.formation.domain.Tag
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface TagRepository : MongoRepository<Tag, ObjectId> {

    fun findByNom(nom: String): Tag?

}
