package com.acenvironnement.formation.repository

import com.acenvironnement.formation.domain.Contact
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ContactRepository : MongoRepository<Contact, ObjectId> {

    fun findByTagsIds(tagId: ObjectId): List<Contact>

    @Query("{ \"adresse\" : { \$elemMatch : {\"ville\" : \"?0\" } } }")
    fun findByNomVille(nom: String): List<Contact>

}
