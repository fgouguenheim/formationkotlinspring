package com.acenvironnement.formation.repository

import com.acenvironnement.formation.domain.HelloWorldMessage
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface HelloWorldRepository : MongoRepository<HelloWorldMessage, ObjectId> {

    fun findByText(text: String)

}
