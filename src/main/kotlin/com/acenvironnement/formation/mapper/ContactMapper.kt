package com.acenvironnement.formation.mapper

import com.acenvironnement.formation.domain.Contact
import com.acenvironnement.formation.domain.ContactADO
import com.acenvironnement.formation.domain.Tag

fun Contact.toADO(tags: Iterable<Tag>): ContactADO {
    return ContactADO(
        id = id,
        nom = nom,
        numTel = numTel,
        adresse = adresse,
        tags = tags.filter { this.tagsIds.contains(it.id) }.map { it.nom },
        dateCreation = dateCreation,
        dateDerniereMaj = dateDerniereMaj
    )
}
