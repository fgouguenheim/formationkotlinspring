package com.acenvironnement.formation.domain

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field

@Document(collection = "hello_world_messages")
data class HelloWorldMessage(

    @Id
    val id: ObjectId? = null,

    @Field("Text")
    val text: String,

    )
