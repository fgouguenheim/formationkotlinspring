package com.acenvironnement.formation.domain

import org.bson.types.ObjectId
import java.time.LocalDateTime

data class ContactADO(

    val id: ObjectId? = null,

    val nom: String,

    val numTel: List<String> = emptyList(),

    val adresse: List<Adresse> = listOf(),

    val tags: List<String> = listOf(),

    val dateCreation: LocalDateTime? = null,

    val dateDerniereMaj: LocalDateTime? = null,

    )

