package com.acenvironnement.formation.domain

import org.bson.types.ObjectId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document(collection = "tags")
data class Tag(

    @Id
    val id: ObjectId? = null,

    val nom: String,

    @CreatedDate
    val dateCreation: LocalDateTime? = null,

    @LastModifiedDate
    val dateDerniereMaj: LocalDateTime? = null,

    )
