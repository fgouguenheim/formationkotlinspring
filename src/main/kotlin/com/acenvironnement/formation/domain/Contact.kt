package com.acenvironnement.formation.domain

import org.bson.types.ObjectId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document(collection = "contacts")
data class Contact(

    @Id
    val id: ObjectId? = null,

    val nom: String,

    val numTel: List<String> = emptyList(),

    val adresse: List<Adresse> = listOf(),

    val tagsIds: List<ObjectId> = listOf(),

    @CreatedDate
    val dateCreation: LocalDateTime? = null,

    @LastModifiedDate
    val dateDerniereMaj: LocalDateTime? = null,

    )

data class Adresse(

    /**
     * Numéro et nom de la voie
     */
    val voie: String? = null,

    /**
     * Code postal
     */
    val codePostal: String? = null,

    /**
     * Ville
     */
    val ville: String? = null,

    )
