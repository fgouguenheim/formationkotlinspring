package com.acenvironnement.formation.config

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.BeanFactory
import org.springframework.beans.factory.NoSuchBeanDefinitionException
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.mongodb.MongoDatabaseFactory
import org.springframework.data.mongodb.core.convert.DbRefResolver
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver
import org.springframework.data.mongodb.core.convert.MappingMongoConverter
import org.springframework.data.mongodb.core.convert.MongoCustomConversions
import org.springframework.data.mongodb.core.mapping.MongoMappingContext
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@Configuration
@EnableMongoRepositories("com.acenvironnement.**.repository")
@Import(value = [MongoAutoConfiguration::class])
class DatabaseConfiguration {

    private val log = LoggerFactory.getLogger(javaClass)

    @Bean
    fun mappingMongoConverter(factory: MongoDatabaseFactory, context: MongoMappingContext, beanFactory: BeanFactory): MappingMongoConverter {
        val dbRefResolver: DbRefResolver = DefaultDbRefResolver(factory)
        val mappingConverter = MappingMongoConverter(dbRefResolver, context)
        try {
            mappingConverter.setCustomConversions(beanFactory.getBean(MongoCustomConversions::class.java))
        } catch (ignore: NoSuchBeanDefinitionException) {
        }
        // Mapper Mongo personnalisé
        mappingConverter.setTypeMapper(CustomMongoFileMapper())
        return mappingConverter
    }
}
