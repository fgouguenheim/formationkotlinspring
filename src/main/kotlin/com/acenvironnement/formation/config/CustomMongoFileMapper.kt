package com.acenvironnement.formation.config

import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper

// On utilise le constructeur de la superclasse en lui spécifiant "typeKey" à null ( = ne pas utiliser de "typeKey"),
// ceci afin d'éviter que Spring Data génère automatiquement le champ "_class" dans les objects MongoDB lorsqu'il les enregistre
class CustomMongoFileMapper : DefaultMongoTypeMapper(null)
