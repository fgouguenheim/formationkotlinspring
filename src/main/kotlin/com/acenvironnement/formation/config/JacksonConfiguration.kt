package com.acenvironnement.formation.config

import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.bson.types.ObjectId
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class JacksonConfiguration {

    @Bean
    fun javaTimeModule() = JavaTimeModule()

    @Bean
    fun jdk8TimeModule() = Jdk8Module()

    /**
     * Support pour la sérialisation correcte du type ObjectId
     */
    @Bean
    fun customizer() = Jackson2ObjectMapperBuilderCustomizer { it.serializerByType(ObjectId::class.java, ToStringSerializer()) }
}


