package com.acenvironnement.formation.service

import com.acenvironnement.formation.domain.Tag
import com.acenvironnement.formation.repository.TagRepository
import org.bson.types.ObjectId
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class TagService(
    private val tagRepository: TagRepository,
) {

    fun findAll(): List<Tag> {
        return tagRepository.findAll()
    }

    fun findById(id: ObjectId): Tag? {
        return tagRepository.findByIdOrNull(id)
    }

    fun save(updateTag: Tag): Tag {
        return updateTag.id?.let {
            tagRepository.findByIdOrNull(updateTag.id)?.let { baseTag ->
                // Modif
                val baseTagUpdated = baseTag.copy(
                    nom = updateTag.nom,
                )
                tagRepository.save(baseTagUpdated)
            }
        } ?:
        // Création
        tagRepository.save(updateTag)
    }

    fun deleteById(id: ObjectId) {
        return tagRepository.deleteById(id)
    }

}
