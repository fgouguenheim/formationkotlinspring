package com.acenvironnement.formation.service

import com.acenvironnement.formation.domain.Contact
import com.acenvironnement.formation.domain.ContactADO
import com.acenvironnement.formation.mapper.toADO
import com.acenvironnement.formation.repository.ContactRepository
import com.acenvironnement.formation.repository.TagRepository
import org.bson.types.ObjectId
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class ContactService(
    private val contactRepository: ContactRepository,
    private val tagRepository: TagRepository,

    ) {

    fun findAll(): List<ContactADO> {
        val contacts = contactRepository.findAll()
        return toContactsADO(contacts)
    }

    fun findByTagId(id: ObjectId): List<ContactADO> {
        val contacts = contactRepository.findByTagsIds(id)
        return toContactsADO(contacts)
    }

    fun findByNomTag(nom: String): List<ContactADO> {
        return tagRepository.findByNom(nom)?.let {
            findByTagId(it.id!!)
        } ?: emptyList()
    }

    fun findByNomVille(nom: String): List<ContactADO> {
        val contacts = contactRepository.findByNomVille(nom)
        return toContactsADO(contacts)
    }

    fun findById(id: ObjectId): ContactADO {
        val contact = contactRepository.findByIdOrNull(id) ?: throw NoSuchElementException()
        return toContactsADO(listOf(contact))[0]
    }

    fun save(updateContact: Contact): Contact {
        return updateContact.id?.let {
            contactRepository.findByIdOrNull(updateContact.id)?.let { baseContact ->
                // Modif
                val baseContactUpdated = baseContact.copy(
                    nom = updateContact.nom,
                    numTel = updateContact.numTel,
                    adresse = updateContact.adresse
                )
                contactRepository.save(baseContactUpdated)
            }
        } ?:
        // Création
        contactRepository.save(updateContact)
    }

    fun deleteById(id: ObjectId) {
        return contactRepository.deleteById(id)
    }

    private fun toContactsADO(contacts: Iterable<Contact>): List<ContactADO> {
        val tags = tagRepository.findAllById(contacts.flatMap { it.tagsIds }.map { it }.toSet())
        return contacts.map { it.toADO(tags) }
    }

}
