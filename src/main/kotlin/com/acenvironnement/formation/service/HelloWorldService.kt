package com.acenvironnement.formation.service

import com.acenvironnement.formation.repository.HelloWorldRepository
import org.springframework.stereotype.Service

@Service
class HelloWorldService(
    private val helloWorldRepository: HelloWorldRepository,
) {

    fun findAll(): String {
        return helloWorldRepository.findAll().joinToString { it.text }
    }

}
