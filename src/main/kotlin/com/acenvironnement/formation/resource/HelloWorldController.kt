package com.acenvironnement.formation.resource

import com.acenvironnement.formation.service.HelloWorldService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/hello")
class HelloWorldController(
    private val helloWorldService: HelloWorldService,
) {

    @GetMapping
    fun findAll(): String {
        return helloWorldService.findAll()
    }
}
