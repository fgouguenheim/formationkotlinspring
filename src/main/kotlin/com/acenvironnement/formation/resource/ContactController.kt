package com.acenvironnement.formation.resource

import com.acenvironnement.formation.domain.Contact
import com.acenvironnement.formation.domain.ContactADO
import com.acenvironnement.formation.service.ContactService
import org.bson.types.ObjectId
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/contacts")
class ContactController(
    private val contactService: ContactService,
) {

    @GetMapping
    fun findAll(): List<ContactADO> {
        return contactService.findAll()
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: ObjectId): ContactADO {
        return contactService.findById(id)
    }

    @GetMapping("/tag/{id}")
    fun findByTagId(@PathVariable id: ObjectId): List<ContactADO> {
        return contactService.findByTagId(id)
    }

    @GetMapping("/tag")
    fun findByNomTag(@RequestParam nom: String): List<ContactADO> {
        return contactService.findByNomTag(nom)
    }

    @GetMapping("/ville")
    fun findByNomVille(@RequestParam nom: String): List<ContactADO> {
        return contactService.findByNomVille(nom)
    }

    @PostMapping
    fun save(@RequestBody contact: Contact): Contact {
        return contactService.save(contact)
    }

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: ObjectId) {
        return contactService.deleteById(id)
    }
}
