package com.acenvironnement.formation.resource

import com.acenvironnement.formation.domain.Tag
import com.acenvironnement.formation.service.TagService
import org.bson.types.ObjectId
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/tags")
class TagController(
    private val tagService: TagService,
) {

    @GetMapping
    fun findAll(): List<Tag> {
        return tagService.findAll()
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: ObjectId): Tag? {
        return tagService.findById(id)
    }

    @PostMapping
    fun save(@RequestBody tag: Tag): Tag {
        return tagService.save(tag)
    }

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: ObjectId) {
        return tagService.deleteById(id)
    }
}
